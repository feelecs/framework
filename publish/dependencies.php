<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

use Hyperf\Contract\StdoutLoggerInterface;
use Feelec\Framework\Log\StdoutLoggerFactory;

$dependencies = [];

$env_name = env('APP_ENV', 'production');

if ($env_name !== 'dev')
{
    $dependencies[StdoutLoggerInterface::class] = StdoutLoggerFactory::class;
}

return $dependencies + [];
