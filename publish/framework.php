<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

return [
	## 默认路由白名单
	'default_route_white' => [
		'/third/callback/wechat',
	],

    ## 自定义的其他路由白名单
    'route_white' => [
        '/third/callback/workwx', '/third/callback/wechat',
    ],

];
