<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Log;

use Psr\Container\ContainerInterface;

class StdoutLoggerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return Log::get('sys');
    }
}
