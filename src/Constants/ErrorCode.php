<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
* @Constants
*/
class ErrorCode extends AbstractConstants
{
	/**
	* @Message("Server Error！")
	*/
	const SERVER_ERROR = 500;

	/**
	* @Message("success")
	*/
	const SUCCESS = 200;

	/**
	* @Message("error")
	*/
	const ERROR = 0;

	/**
	* @Message("资源未找到")
	*/
	const URI_NOT_FOUND = 100010;

	/**
	* @Message("请求方法错误")
	* @HttpCode("405")
	*/
	const INVALID_HTTP_METHOD = 100011;

	/**
	* @Message("用户不存在")
	*/
	const USER_NOT_EXIST = 100200;
}
