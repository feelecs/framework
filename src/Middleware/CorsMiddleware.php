<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Middleware;

use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class CorsMiddleware implements MiddlewareInterface
{
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$cors_origin = config('framework.cors_origin', '*');

		$origin       = 'null';

		if ($cors_origin === '*')
		{
			$origin = '*';
		}
		else
		{
			$request_origin = $request->getHeaderLine('Origin');

			in_array($request_origin, explode(',', $cors_origin), true) && $origin = $request_origin;
		}

		$response = Context::get(ResponseInterface::class);

		$response = $response->withHeader('Access-Control-Allow-Origin', $origin)
			->withHeader('Access-Control-Allow-Credentials', 'true')
			->withHeader(
				'Access-Control-Allow-Headers',
				'Authorization,Accept,Content-Type,Origin,User-Agent,X-Requested-With,Login-Client'
			)
			->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

		Context::set(ResponseInterface::class, $response);

		if (strtoupper($request->getMethod()) === 'OPTIONS')
		{
			return $response;
		}

		return $handler->handle($request);
	}
}
