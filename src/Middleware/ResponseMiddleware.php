<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Middleware;

use Feelec\Framework\Traits\Route;
use Hyperf\Contract\ConfigInterface;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
* 格式化响应数据中间件
* Class ResponseMiddleware.
*/
class ResponseMiddleware implements MiddlewareInterface
{
    use Route;

    /**
	* @var string 路由白名单
	*/
    protected $route_white;

    /**
	* @var ContainerInterface 容器
	*/
    protected $container;

    public function __construct(ContainerInterface $container, ConfigInterface $config)
    {
        $this->container         = $container;

        $this->route_white = $config->get('framework.default_route_white', []);
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        if ($this->whiteListAuth($this->route_white))
		{
            return $response;
        }

        return $this->formatStream($response);
    }


    protected function formatStream(ResponseInterface $response): ResponseInterface
    {
        $old_stream = json_decode($response->getBody()->getContents(), true) ?? [];

        $http_code  = $response->getStatusCode();

        $format     = getResponseDataFormat($http_code, '', $old_stream);

        $new_stream = new SwooleStream(json_encode($format, JSON_UNESCAPED_UNICODE));

        return $response->withBody($new_stream);
    }
}
