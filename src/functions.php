<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;

if (! function_exists('getReadFileName'))
{
    /**
	* 取出某目录下所有php文件的文件名.
	* @param string $path 文件夹目录
	* @return array 文件名
	*/
    function getReadFileName(string $path): array
    {
        $data = [];
        if (! is_dir($path)) {
            return $data;
        }

        $files = scandir($path);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..', '.DS_Store'])) {
                continue;
            }
            $data[] = preg_replace('/(\w+)\.php/', '$1', $file);
        }
        return $data;
    }
}

if (! function_exists('getResponseDataFormat'))
{
    function getResponseDataFormat($code, string $message = '', array $data = []): array
    {
        return [
            'code' => $code,
            'msg'  => $message,
            'data' => $data,
        ];
    }
}

if (! function_exists('isDiRequestInit'))
{
    function isDiRequestInit(): bool
    {
        try
        {
            ApplicationContext::getContainer()->get(RequestInterface::class)->input('test');

            $res = true;
        }
		catch (\TypeError $e)
		{
            $res = false;
        }

        return $res;
    }
}
