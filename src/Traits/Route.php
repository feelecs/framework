<?php

declare(strict_types=1);

namespace Feelec\Framework\Traits;

use FastRoute\Dispatcher;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\Utils\ApplicationContext;
use Feelec\Framework\Constants\ErrorCode;
use Feelec\Framework\Exception\FeelecException;

trait Route
{
    protected function formatRoute(): string
    {
        $request    = ApplicationContext::getContainer()->get(RequestInterface::class);

        $dispatched = $request->getAttribute(Dispatched::class);

        $route = $request->getUri()->getPath();

        switch ($dispatched->status)
        {
            case Dispatcher::NOT_FOUND:

				throw new FeelecException(ErrorCode::URI_NOT_FOUND);

            case Dispatcher::METHOD_NOT_ALLOWED:

				throw new FeelecException(ErrorCode::INVALID_HTTP_METHOD);

            case Dispatcher::FOUND:

	            $route = $dispatched->handler->route;

                if (strpos($route, '{') === false)
				{
                    break;
                }

	            $route = preg_replace('/:.*?}($|\/)/', '}/', $dispatched->handler->route);

	            $route = rtrim($route, '/');
        }

        return $route;
    }


    protected function whiteListAuth(array $whites = []): bool
    {
        if (empty($whites))
		{
            return false;
        }

        $route = $this->formatRoute();

        return in_array($route, $whites);
    }
}
