<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Exception\Handler;

use Feelec\Framework\Exception\FeelecException;
use Hyperf\Config\Annotation\Value;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\ApplicationContext;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class FeelecExceptionHandler extends ExceptionHandler
{
	/**
	* @Value("app_name")
	* @var $appName
	*/
	private $appName;

	public function handle(Throwable $throwable, ResponseInterface $response)
	{
		$responseContents = $response->getBody()->getContents();

		$responseContents = json_decode($responseContents, true);

		if (!empty($responseContents['error']))
		{
			$port = null;

			$config = ApplicationContext::getContainer()->get(ConfigInterface::class);

			$servers = $config->get('server.servers');

			foreach ($servers as $server)
			{
				if ($server['name'] === 'jsonrpc-http')
				{
					$port = $server['port'];

					break;
				}
			}

			$responseContents['error']['message'] .= " - {$this->appName}:{$port}";
		}

		$data = json_encode($responseContents, JSON_UNESCAPED_UNICODE);

		return $response->withStatus(200)->withBody(new SwooleStream($data));
	}


	public function isValid(Throwable $throwable): bool
	{
		return $throwable instanceof FeelecException;
	}
}
