<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
declare(strict_types=1);

namespace Feelec\Framework\Exception;

use Feelec\Framework\Constants\ErrorCode;
use Hyperf\Server\Exception\ServerException;


class FeelecException extends ServerException
{
    public function __construct(int $code = 0, string $message = null, \Throwable $previous = null)
    {
        if (is_null($message))
		{
            $message = ErrorCode::getMessage($code);

            if (!$message && class_exists(ErrorCode::class))
			{
                $message = ErrorCode::getMessage($code);
            }
        }

        parent::__construct($message, $code, $previous);
    }
}
