<?php
/**
* 成都菲莱克斯科技有限公司出品，未经授权许可不得使用！
* @Author Yishu
* @Link   https://www.feeldesk.cn
*/
namespace Feelec\Framework;

use Feelec\Framework\Exception\Handler\FeelecExceptionHandler;
use Feelec\Framework\Middleware\ResponseMiddleware;
use Feelec\Framework\Middleware\CorsMiddleware;
use Hyperf\ExceptionHandler\Listener\ErrorExceptionHandler;


class ConfigProvider
{
	public function __invoke(): array
	{
		$dependencies = $this->dependencies();

		return [
			// 合并到  config/autoload/dependencies.php 文件
			'dependencies' => array_merge($dependencies, []),
			// 合并到  config/autoload/exceptions.php 文件
			'exceptions' => [
				'handler' => [
					'http' => [
						FeelecExceptionHandler::class
					],
					'jsonrpc-http' => [
						FeelecExceptionHandler::class
					],
				],
			],
			// 合并到  config/autoload/middlewares.php 文件
			'middlewares' => [
				'http' => [
					CorsMiddleware::class,
					ResponseMiddleware::class,
				],
				'jsonrpc-http' => [
					ResponseMiddleware::class,
				],
			],
			// 合并到  config/autoload/annotations.php 文件
			'annotations' => [
				'scan' => [
					'paths' => [
						__DIR__,
					],
				],
			],
			// 默认 Command 的定义，合并到 Hyperf\Contract\ConfigInterface 内，换个方式理解也就是与 config/autoload/commands.php 对应
			'commands' => [],
			// 与 commands 类似
			'listeners' => [
				ErrorExceptionHandler::class,
			],
			// 组件默认配置文件，即执行命令后会把 source 的对应的文件复制为 destination 对应的的文件
			'publish' => [
				[
					'id' => 'framework',
					## 描述
					'description' => 'framework配置',
					## 对应的配置文件路径，建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
					'source'      => __DIR__ . '/../publish/framework.php',
					## 复制为这个路径下的该文件
					'destination' => BASE_PATH . '/config/autoload/framework.php',
				],
				[
					'id'          => 'dependencies',
					'description' => '依赖配置',
					'source'      => __DIR__ . '/../publish/dependencies.php',
					'destination' => BASE_PATH . '/config/autoload/dependencies.php',
				],
			],
			// 亦可继续定义其它配置，最终都会合并到与 ConfigInterface 对应的配置储存器中
		];
	}


	/**
	* 模型服务与契约的依赖配置.
	* @param string $path 契约与服务的相对路径
	* @return array 依赖数据
	*/
	protected function dependencies(string $path = 'app'): array
	{
		$services    = getReadFileName(BASE_PATH . '/' . $path . '/Service');

		$spacePrefix = ucfirst($path);

		$dependencies = [];

		foreach ($services as $service)
		{
			$dependencies[$spacePrefix . '\\Contract\\' . $service . 'Contract'] = $spacePrefix . '\\Service\\' . $service;
		}

		return $dependencies;
	}
}
